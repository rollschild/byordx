import { createContext, useContext } from "react";

export const CREATE_NOTE = "CREATE_NOTE";
export const UPDATE_NOTE = "UPDATE_NOTE";
export const OPEN_NOTE = "OPEN_NOTE";
export const CLOSE_NOTE = "CLOSE_NOTE";

export const initialState = {
  notes: {},
  openNoteId: undefined,
  isLoading: false,
};

export const Context = createContext(null);
/*
 * Reudcer
 */
const handlers = {
  [CREATE_NOTE]: (state, action) => {
    if (!action?.payload?.id) {
      return {
        ...state,
        isLoading: true,
      };
    }
    const { id } = action.payload;
    const newNote = {
      id,
      content: "",
    };
    return {
      ...state,
      isLoading: false,
      notes: {
        ...state.notes,
        [id]: newNote,
      },
      openNoteId: id,
    };
  },
  [UPDATE_NOTE]: (state, action) => {
    const { id, content } = action.payload;
    const editedNote = {
      ...state.notes[id],
      content,
    };
    return {
      ...state,
      notes: {
        ...state.notes,
        [id]: editedNote,
      },
    };
  },
  [OPEN_NOTE]: (state, action) => {
    return {
      ...state,
      openNoteId: action.payload.id,
    };
  },
  [CLOSE_NOTE]: (state, action) => {
    return {
      ...state,
      openNoteId: undefined,
    };
  },
};

// reducer should be a **pure** function
export const reducer = (state = initialState, action) => {
  return handlers[action.type] ? handlers[action.type](state, action) : state;
};

/*
 * the Store
 */
const validateAction = (action) => {
  if (!action || typeof action !== "object" || Array.isArray(action)) {
    throw new Error("Action must be an object!");
  }
  if (typeof action.type === "undefined") {
    throw new Error("Action must have a type field!");
  }
};

// apparently, the store takes a reducer as an input
export const createStore = (reducer, middleware) => {
  let state;
  const subscribers = [];
  const originalDispatch = (action) => {
    validateAction(action);
    state = reducer(state, action);
    subscribers.forEach((handler) => handler());
  };
  const getState = () => state;

  const store = {
    notify: () => {
      subscribers.forEach((handler) => handler());
    },
    dispatch: originalDispatch,
    getState,
    subscribe: (handler) => {
      subscribers.push(handler);
      // returns a function to **UN-subscribe**
      return () => {
        const index = subscribers.indexOf(handler);
        if (index > 0) {
          subscribers.splice(index, 1);
        }
      };
    },
  };

  // if the middleware decides to dispatch a new action, that new action goes by through the middleware
  if (middleware) {
    const dispatch = (action) => store.dispatch(action);
    store.dispatch = middleware({
      dispatch,
      getState,
    })(originalDispatch);
  }
  originalDispatch({ type: "@@redux/INIT" });
  return store;
};

export const createStoreHook = (context) => {
  const useReduxContext = () => useContext(context);
  return function useStore() {
    const { store } = useReduxContext();
    return store;
  };
};

export const createDispatchHook = (context) => {
  const useStore = createStoreHook(context);
  return function useDispatch() {
    const store = useStore();
    return store.dispatch;
  };
};

export const createSelectorHook = (context) => {
  const useReduxContext = () => useContext(context);
  return function useSelector(selector) {
    const { store } = useReduxContext();
    const selectedState = selector(store.getState());
    return selectedState;
  };
};

/*
 * Encapsulates the subscription logic for connecting a component to the redux store
 */
/*
 * type Listener = {
 *   callback: function
 *   next: Listener | undefined
 *   prev: Listener | undefined
 * }
 */
export const createListenerCollection = () => {
  let first;
  let last;

  return {
    clear: () => {
      first = undefined;
      last = undefined;
    },
    notify: () => {
      let listener = first;
      while (listener) {
        listener.callback();
        listener = listener.next;
      }
    },
    get: () => {
      const listeners = [];
      let listener = first;
      while (listener) {
        listeners.push(listener);
      }
      return listeners;
    },
    subscribe: (callback) => {
      let isSubscribed = true;
      // append callback to the end of the linked list
      last = {
        callback,
        next: undefined,
        prev: last,
      };
      let listener = last;
      if (listener.prev) {
        listener.prev.next = listener;
      } else {
        first = listener;
      }

      return function unsubscribe() {
        if (!isSubscribed || !first) {
          return;
        }
        if (listener.next) {
          listener.next.prev = listener.prev;
        } else {
          last = listener.prev;
        }
        if (listener.prev) {
          listener.prev.next = listener.next;
        } else {
          first = listener.next;
        }
      };
    },
  };
};

const nullListeners = {
  notify: () => {},
  get: () => [],
};

export const createSubscription = (store) => {
  // the unsubscribe function
  let unsubscribe;
  let listeners = nullListeners;

  const handleChangeWrapper = () => {
    if (subscription.onStateChange) {
      subscription.onStateChange();
    }
  };

  const trySubscribe = () => {
    if (!unsubscribe) {
      unsubscribe = store.subscribe(handleChangeWrapper);
      listeners = createListenerCollection();
    }
  };
  const tryUnsubscribe = () => {
    if (unsubscribe) {
      unsubscribe();
      unsubscribe = undefined;
      listeners.clear();
      listeners = nullListeners;
    }
  };
  const notifyNestedSubs = () => {
    listeners.notify();
  };
  const isSubscribed = () => {
    return !!unsubscribe;
  };

  const subscription = {
    trySubscribe,
    tryUnsubscribe,
    handleChangeWrapper,
    notifyNestedSubs,
    isSubscribed,
    getListeners: () => listeners,
  };

  return subscription;
};

export const applyMiddleware =
  (...middlwareList) =>
  (store) => {
    if (middlwareList.length === 0) {
      return (dispatch) => {
        return dispatch;
      };
    }
    if (middlwareList.length === 1) {
      return middlwareList[0](store);
    }
    const boundMiddlewareList = middlwareList.map((middleware) =>
      middleware(store),
    );

    // a(b(c(d(nextDispatchFunction))))
    // returns a function which takes `store` as arg, which returns a function that takes `dispatch` as arg, which returns a function that takes `action` as an arg
    return boundMiddlewareList.reduce(
      (prev, curr) => (next) => prev(curr(next)),
    );
  };
