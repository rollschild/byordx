import React, { createContext, useEffect, useState } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import {
  applyMiddleware,
  CLOSE_NOTE,
  createDispatchHook,
  createSelectorHook,
  createStore,
  createStoreHook,
  CREATE_NOTE,
  OPEN_NOTE,
  reducer,
  UPDATE_NOTE,
} from "./redux";

const root = ReactDOM.createRoot(document.getElementById("root"));

// const delayMiddleware = () => (next) => (action) => {
//   setTimeout(() => next(action), 1000);
// };
const loggingMiddleware =
  ({ getState }) =>
  (next) =>
  (action) => {
    console.info("before:", getState());
    console.info("action:", action);
    const result = next(action);
    console.info("after:", getState());
    return result;
  };
const thunkMiddleware =
  ({ dispatch, getState }) =>
  (next) =>
  (action) => {
    return typeof action === "function"
      ? action(dispatch, getState)
      : next(action);
  };
const store = createStore(
  reducer,
  applyMiddleware(thunkMiddleware, loggingMiddleware),
);
const noteContext = createContext(store);
const useStore = createStoreHook(noteContext);
const useDispatch = createDispatchHook(noteContext);
const useSelector = createSelectorHook(noteContext);

// fake API
const createFakeAPI = () => {
  let id = 0;
  const createNote = () =>
    new Promise((resolve) =>
      setTimeout(() => {
        id++;
        resolve({
          id: `${id}`,
        });
      }, 1000),
    );
  return { createNote };
};
const api = createFakeAPI();

const NoteEditor = ({ note, onChangeNote, onCloseNote }) => (
  <div>
    <div>
      <textarea
        className="editor-content"
        autoFocus
        value={note.content}
        onChange={(event) => onChangeNote(note.id, event.target.value)}
        rows={10}
        cols={80}
      />
    </div>
    <button className="editor-button" onClick={onCloseNote}>
      Close
    </button>
  </div>
);

const NoteTitle = ({ note }) => {
  const title = note.content.split("\n")[0].replace(/^\s+|\s+$/g, "");
  return title ? <span>{title}</span> : <i>Untitled</i>;
};

const NoteLink = ({ note, onOpenNote }) => (
  <li className="note-list-item">
    <a href="#" onClick={() => onOpenNote(note.id)}>
      <NoteTitle note={note} />
    </a>
  </li>
);

const NoteList = ({ notes, onOpenNote }) => (
  <ul className="note-list">
    {Object.entries(notes).map(([noteId, note]) => (
      <NoteLink key={noteId} note={note} onOpenNote={onOpenNote} />
    ))}
  </ul>
);

const createNote = () => {
  return (dispatch) => {
    dispatch({
      type: CREATE_NOTE,
    });

    // async API
    api.createNote().then(({ id }) => {
      dispatch({
        type: CREATE_NOTE,
        payload: {
          id,
        },
      });
    });
  };
};

const NoteApp = () => {
  const notes = useSelector((state) => state.notes);
  const openNoteId = useSelector((state) => state.openNoteId);
  const dispatch = useDispatch();

  const onAddNote = () => dispatch(createNote());
  // const onAddNote = () => createNote()(dispatch);

  const onChangeNote = (id, content) =>
    dispatch({
      type: UPDATE_NOTE,
      payload: {
        id,
        content,
      },
    });
  const onOpenNote = (id) =>
    dispatch({
      type: OPEN_NOTE,
      payload: {
        id,
      },
    });
  const onCloseNote = () =>
    dispatch({
      type: CLOSE_NOTE,
    });

  return (
    <div>
      {openNoteId ? (
        <NoteEditor
          note={notes[openNoteId]}
          onChangeNote={onChangeNote}
          onCloseNote={onCloseNote}
        />
      ) : (
        <div>
          <NoteList notes={notes} onOpenNote={onOpenNote} />
          <button className="editor-button" onClick={onAddNote}>
            New Note
          </button>
        </div>
      )}
    </div>
  );
};

const Provider = ({ store, children }) => {
  const [contextValue, setContextValue] = useState({
    store,
    counter: 0,
  });

  useEffect(() => {
    const forceUpdate = () =>
      setContextValue({
        store: { ...contextValue.store },
        counter: contextValue.counter + 1,
      });
    const unsubscribe = store.subscribe(forceUpdate);

    return () => unsubscribe();
  }, []);

  const Context = noteContext;
  return <Context.Provider value={contextValue}>{children}</Context.Provider>;
};

function renderApp() {
  root.render(
    <React.StrictMode>
      <Provider store={store}>
        <NoteApp />
      </Provider>
    </React.StrictMode>,
  );
}

renderApp();
