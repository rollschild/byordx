# Build Your Own Redux

- Build a minimal Redux from scratch, in a simple note-taking React app.
- Supports middleware and thunk
- Thunk:
  - a function that is wrapping some work to be done later
- Action creators:
  - functions that return actions to be dispatched
- make sure that your actions are descriptions of side effects and not actually side effects
